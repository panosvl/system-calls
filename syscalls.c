/**
 * syscalls.c
 *
 * created by S.T.P. team:
 *
 * Stefania Nimi
 *
 * Tasos Tsoulkas
 *
 * Panagiotis Vlachos 
 *
 * Tracks system calls of various UNIX shell commands.
 * Usage: ./syscalls path/to/textfile1.txt path/to/textfile2.txt
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_SIZE 1000

int main(int argc, char *argv[]) {

	int Lhor = 5;
	int Lver = 5;
	int row, col, temp_max, rowstart, colstart, i, j, LA, LB, M, N, maxv;
	int A[MAX_SIZE];
	int B[MAX_SIZE];
	int D[MAX_SIZE][MAX_SIZE];
	int acc_cost[MAX_SIZE][MAX_SIZE];

	float penalty = 1/3;
	
	if (argc != 3)
	{
		printf("There was an error with your parameters\n");
		printf("Try running ./executable file1.txt file2.txt\n");
	}

	char* name1;
	name1 = malloc(sizeof(char));
	name1 = argv[1];
	char* name2;
	name2 = malloc(sizeof(char));
	name2 = argv[2];

	// ----------------FILE MANIPULATION-------------
	// FILE 1
	FILE *fp1 = fopen(name1, "r");
	if (fp1 == NULL)
	{
		perror("Error opening file 1.");
		return 1;
	}
	
	i = 0;
	fscanf(fp1, "%d", &A[i]);
	while (!feof (fp1))
    {  
	    fscanf (fp1, "%d", &A[i]);
	    i++;     
    }

    LA = i;

	fclose(fp1);

	// FILE 2
	FILE *fp2 = fopen(argv[2], "r");
	if (fp2 == NULL)
	{
		perror("Error opening file 2.");
	}
	
	i = 0;
	fscanf(fp2, "%d", &B[i]);
	while (!feof (fp2))
    {  
	    fscanf (fp2, "%d", &B[i]);
	    i++;     
    }

    LB = i;

	fclose(fp2);	

	// ------------END OF FILE MANIPULATION----------

	// -----------------INITIALIZE-------------------
	
	// initializing array D filled with 0
	M = LA++;
	N = LB++;
	for (i = 0; i < M; i++)
	{
		for (j = 0; j < N; j++)
		{
			D[i][j] = 0;
		}
	}
	// end of array D initialization

	for (i = 0; i < LA; i++)
	{
		for (j = 0; j < LB; j++)
		{
			if (A[i] == B[j])
			{
				D[i+1][j+1] = 1;
			}
			else
			{
				D[i+1][j+1] = -penalty; 
			}
		}
	}
	// --------------END OF INITIALIZE--------------

	// initializing array acc_cost[M, N] filled with 0
	for (i = 0; i < M; i++)
	{
		for (j = 0; j < N; j++)
		{
			acc_cost[i][j] = 0;
		}
	}

	
	// --------------------PROCESSING---------------
	for (i = 1; i < M; i++)
	{
		for (j = 1; j < N; j++)
		{
			temp_max = D[i][j];
			// diagonal processing
			if ((acc_cost[i - 1][j - 1] + D[i][j]) > temp_max)
			{
				temp_max = acc_cost[i-1][j-1] + D[i][j];
			}

			// processing columns
			if ((i-Lver) >= 1)
			{
				rowstart = i - Lver;
			}
			else
			{
				rowstart = 1;
			}

			// some magic that I cannot understand
			for (row = rowstart; row < i-1; row ++) 
			{
				if ((acc_cost[row][j] - (1 + (penalty)*(i-row))) > temp_max)
				{
					temp_max = acc_cost[row][j] - (1 + (penalty) * (i-row));
				}
			}

			// scanning rows (elements:(i,1),(i,2),...,(i,j-1))
			if ((j - Lhor) >= 1)
			{
				colstart = j - Lhor;
			}
			else
			{
				colstart = 1;
			}
			
			for (col = colstart; col < j-1; col++)
			{
				if (acc_cost[i][col] - (1 + (penalty) * (j - col)) > temp_max)
				{
					temp_max = acc_cost[i][col] - (1 + (penalty)*(j-col));
				}
			}

			// findin the maximum similarity
			if (temp_max > 0)
			{
				acc_cost[i][j] = temp_max;
			}

		}
	}
	// ----------------END OF PROCESSING---------------

	// finding the maximum value in acc_cost array
	maxv = acc_cost[0][0];
	for (i = 0; i < LA; i++)
	{
		for (j = 0; j < LB; j++)
		{
			if (acc_cost[i][j] > maxv)
			{
				maxv = acc_cost[i][j];
			}
		}
	}

	// -------------PRINTING OUT THE RESULTS-------------
	printf("%d\n", maxv);

	return 0;
}